<?php

/*
Template Name: Paid Search Template

*/
add_filter( 'fl_topbar_enabled', '__return_false' );
add_filter( 'fl_fixed_header_enabled', '__return_false' );
add_filter( 'fl_header_enabled', '__return_false' );
add_filter( 'fl_footer_enabled', '__return_false' );
get_header();

?>
<style>
.footer-1-top{display:none;}
.fl-content {
    margin: 0px 0 !important;
}
</style>

<div class="fl-content-full container">
	<div class="row">
		<div class="fl-content col-md-12" style="margin:0px;">
			<?php
			
			echo do_shortcode('[fl_builder_insert_layout slug="landing-page-header"]');
			 
			echo do_shortcode('[fl_builder_insert_layout slug="commercial-landing-page-template	"]');
			
			echo do_shortcode('[fl_builder_insert_layout slug="landing-page-footer"]');

			?>
		</div>
	</div>
</div>

<?php get_footer(); ?>

